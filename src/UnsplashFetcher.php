<?php

namespace Drupal\unsplash;

use Drupal\Core\Cache\CacheBackendInterface;
use Unsplash\HttpClient;

/**
 * Fetch, and cache, data from the Unsplash API.
 */
class UnsplashFetcher {

  /**
   * Cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The unsplash client.
   *
   * @var \Unsplash\HttpClient
   */
  protected $unsplashClient;

  /**
   * Constructs a UnsplashFetcher object.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   A cache bin for storing fetched API responses.
   * @param \Unsplash\HttpClient $unsplashClient
   *   The unsplash PHP SDK client.
   */
  public function __construct(CacheBackendInterface $cacheBackend, HttpClient $unsplashClient) {
    $this->cache = $cacheBackend;
    $this->unsplashClient = $unsplashClient;
  }

  /**
   * Fetch results based on a search term.
   *
   * @param string $searchTerm
   *   The search term.
   *
   * @return void
   */
  public function fetch($searchTerm) {
    if ($this->cache && $cached_response = $this->cache->get($searchTerm)) {
      return $cached_response->data;
    }


  }

}